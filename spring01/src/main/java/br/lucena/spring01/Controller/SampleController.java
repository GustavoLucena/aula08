package br.lucena.spring01.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //indica que esta classe será um controller REST
@RequestMapping("/exemplo") //indica que esta rota é atendida nesta classe
public class SampleController {
    
    @GetMapping("/hello") //1 - a requisição será do tipo GET. 2 - Qual rota será usada
    public ResponseEntity<String> getOla(){ //Devolve uma resposta padrão web com uma String
        return ResponseEntity.ok("Olá Mundo!"); //ok - status 200
    }

}
